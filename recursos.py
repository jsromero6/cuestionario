class Actividad:
    identificador = ''
    nombre = ''
    preguntas = []

    def __init__(self, identificador, nombre):
        self.identificador = identificador
        self.nombre = nombre

nombre = input('Digite nombre: ')
identificador = input('Digite identificación: ')
salida = Actividad(identificador, nombre)

class PreguntaSeleccion(Actividad):
    numero = 0
    texto = ''
    opcion1 = ''
    opcion2 = ''
    opcion3 = ''
    respuesta = ''

    def __init__(self, numero, texto, opcion1, opcion2, opcion3, respuesta):
        self.numero = numero
        self.texto = texto
        self.opcion1 = opcion1
        self.opcion2 = opcion2
        self.opcion3 = opcion3
        self.respuesta = respuesta

    def __str__(self):
        return f'{self.texto},{self.opcion1}'

numero = 1
n = numero
while n<=3:
    texto = input(f'Digite la pregunta {n}: ')
    opcion1 = input('1. ')
    opcion2 = input('2. ')
    opcion3 = input('3. ')
    respuesta = input('¿Cuál es el item correcto? (1 - 2 - 3): ')
    n+=1